#include <iostream>
#include "sort_students.h"

int main() {
    Student a;
    a.surname = "aa";
    a.name = "a";
    a.year = 1997;
    a.month = 11;
    a.day = 26;

    Student b;
    b.surname = "bb";
    b.name = "b";
    b.year = 1997;
    b.month = 12;
    b.day = 31;

    Student c;
    c.surname = "cc";
    c.name = "c";
    c.year = 1996;
    c.month = 10;
    c.day = 25;

    Student d;
    d.surname = "aa";
    d.name = "d";
    d.year = 1997;
    d.month = 11;
    d.day = 26;

    Student e;
    e.surname = "aa";
    e.name = "a";
    e.year = 1997;
    e.month = 11;
    e.day = 25;

    Student f;
    f.surname = "ff";
    f.name = "f";
    f.year = 1996;
    f.month = 10;
    f.day = 25;

    std::vector<Student> students = {a, b, c, d, e, f};
    SortStudents(&students, SortType::kByName);
    for (auto s : students) {
        std::cout << s.surname << " " << s.name << " " << s.year << " " << s.month << " " << s.day << std::endl;
    }
    std::cout << std::endl;
    SortStudents(&students, SortType::kByDate);
    for (auto s : students) {
        std::cout << s.year << " " << s.month << " " << s.day << " " << s.surname << " " << s.name << std::endl;
    }

    return 0;
}
