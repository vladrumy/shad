#pragma once

#include <vector>
#include <stdexcept>

void FilterEven(std::vector<int> *data) {
    int odd_num = 0;
    for (auto i = 0; i < data->size(); ++i) {
        if ((*data)[i] % 2) {
            ++odd_num;
        } else if (odd_num > 0) {
            std::swap((*data)[i], (*data)[i - odd_num]);
        }
    }

    for (int i = 0; i < odd_num; ++i) {
        data->pop_back();
    }
}
