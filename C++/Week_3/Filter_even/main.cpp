#include <iostream>
#include "filter_even.h"

int main() {
    std::vector<int> v = {3, 2, 4, 1, 2, 5, 7, 8, 9};
    FilterEven(&v);
    for (auto elem : v) {
        std::cout << elem << " ";
    }
    std::cout << std::endl;

    return 0;
}
