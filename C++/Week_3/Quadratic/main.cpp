#include <iostream>
#include "quadratic.h"

int main() {
    double a, b, c;
    std::cin >> a >> b >> c;
    auto ans = SolveQuadratic(a, b, c);
    std::cout << ans.first << " " << ans.second << std::endl;

    return 0;
}
