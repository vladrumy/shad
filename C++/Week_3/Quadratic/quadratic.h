#pragma once

#include <cmath>
#include <stdexcept>


enum class RootCount { kZero, kOne, kTwo, kInf };

struct Roots {
    RootCount count;
    double first, second;
};

Roots SolveQuadratic(int a, int b, int c) {
    auto a_ = static_cast<double> (a);
    auto b_ = static_cast<double> (b);
    auto c_ = static_cast<double> (c);
    Roots ans;
    double determinant = b_ * b_ - 4 * a_ * c_;
    if (a_ == 0) {
        if (b_ == 0) {
            if (c_ == 0) {
                ans.first = std::numeric_limits<double>::infinity();
            }
        } else {
            ans.first = -c_ / b_;
        }
    } else if (determinant == 0) {
        ans.first = -b_ / 2 / a_;
    } else if (determinant > 0){
        ans.first = (-b_ - sqrt(determinant)) / 2 / a_;
        ans.second = (-b_ + sqrt(determinant)) / 2 / a_;
    }

    return ans;
}
