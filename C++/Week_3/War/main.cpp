#include <iostream>
#include "war.h"

int main() {
    std::array<int, 5> first = {0, 1, 2, 3, 6};
    std::array<int, 5> second = {4, 7, 8, 9, 5};
    auto ans = SimulateWarGame(first, second);
    std::cout << ans.winner << " " << ans.turn << std::endl;

    return 0;
}
