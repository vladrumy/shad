#pragma once

#include <array>
#include <stdexcept>
#include <queue>

enum Winner { kFirst, kSecond, kNone };

struct GameResult {
    Winner winner;
    int turn;
};

GameResult SimulateWarGame(const std::array<int, 5>& first_deck,
                           const std::array<int, 5>& second_deck) {
    GameResult ans;
    std::queue<int> first, second;
    for (auto card : first_deck) {
        first.push(card);
    }
    for (auto card : second_deck) {
        second.push(card);
    }
    int turn = 0;
    while (turn < 1e6 && !first.empty() && !second.empty()) {
        if ((first.front() > second.front() && !(first.front() == 9 && second.front() == 0)) || (first.front() == 0 && second.front() == 9)) {
            first.push(first.front());
            first.push(second.front());
        } else {
            second.push(first.front());
            second.push(second.front());
        }
        first.pop();
        second.pop();
        ++turn;
    }
    ans.turn = turn;
    if (second.empty()) {
        ans.winner = kFirst;
    } else if (first.empty()) {
        ans.winner = kSecond;
    } else {
        ans.winner = kNone;
    }

    return ans;
}
