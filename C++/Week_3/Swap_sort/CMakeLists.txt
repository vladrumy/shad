cmake_minimum_required(VERSION 3.21)
project(Swap_sort)

set(CMAKE_CXX_STANDARD 20)

add_executable(Swap_sort main.cpp swap_sort.h)
