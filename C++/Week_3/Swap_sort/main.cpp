#include <iostream>
#include "swap_sort.h"

int main() {
    int a, b, c;
    std::cin >> a >> b >> c;
    Swap(&a, &b);
    std::cout << a << " " << b << std::endl;
    Sort3(&a, &b, &c);
    std::cout << a << " " << b << " " << c << std::endl;

    return 0;
}
