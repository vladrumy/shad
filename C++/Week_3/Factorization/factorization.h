#pragma once

#include <utility>
#include <vector>
#include <stdexcept>

std::vector<std::pair<int64_t, int>> Factorize(int64_t x) {
    std::vector<std::pair<int64_t, int>> ans;
    int divider = 2;
    while (x > 1) {
        while (x % divider == 0) {
            if (!ans.empty() && ans[ans.size() - 1].first == divider) {
                ++ans[ans.size() - 1].second;
            } else {
                ans.emplace_back(divider, 1);
            }
            x /= divider;
        }
        ++divider;
    }

    return ans;
}
