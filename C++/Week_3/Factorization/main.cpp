#include <iostream>
#include "factorization.h"

int main() {
    int64_t x;
    std::cin >> x;
    auto factor = Factorize(x);
    for (auto elem : factor) {
        std::cout << elem.first << " " << elem.second << std::endl;
    }

    return 0;
}
