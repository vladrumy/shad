#include <iostream>
#include "point_triangle.h"

int main() {
    Point a, b, c, pt;
    std::cin >> a.x >> a.y >> b.x >> b.y >> c.x >> c.y >> pt.x >> pt.y;
    Triangle t;
    t.a = a;
    t.b = b;
    t.c = c;
    std::cout << IsPointInTriangle(t, pt) << std::endl;
    return 0;
}
