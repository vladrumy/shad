#pragma once

#include <stdexcept>

struct Point {
    int x, y;
};

struct Triangle {
    Point a, b, c;
};

struct Line {
//    p * x + q * y + r == 0
    double p, q = 1, r;
//    if x != const, q = 1
//    else q = 0, p = 1
};

Line LineCoef(Point a, Point b) {
    Line ans;
    if (a.x == b.x) {
        if (a.y == b.y) {
            return ans;
        }
        ans.p = 1;
        ans.q = 0;
        ans.r = -a.x;
    } else {
        ans.p = (b.y - a.y) / (a.x - b.x);
        ans.r = (a.y * b.x - a.x * b.y) / (a.x - b.x);
    }

    return ans;
}

bool IsOnLine(Point a, Point b, Point c) {
    auto line = LineCoef(a, b);
    if (line.p * c.x + line.q * c.y + line.r == 0) {
        return true;
    }

    return false;
}

bool IsOnSegment(Point a, Point b, Point c) {
    if (!IsOnLine(a, b, c)) {
        return false;
    }

    return ((a.x - c.x) * (c.x - b.x) >= 0);
}

bool IsSameSide(Point a, Point b, Point c, Point d) {
    auto line = LineCoef(a, b);
    int c_side = line.p * c.x + line.q * c.y + line.r;
    int d_side = line.p * d.x + line.q * d.y + line.r;

    return (c_side * d_side >= 0);
}

inline bool IsPointInTriangle(const Triangle& t, const Point& pt) {
    if (!IsSameSide(t.a, t.b, t.c, pt)) {
        return false;
    }
    if (!IsSameSide(t.a, t.c, t.b, pt)) {
        return false;
    }
    if (!IsSameSide(t.b, t.c, t.a, pt)) {
        return false;
    }

    return true;
}
