#pragma once

#include <map>
#include <tuple>
#include "sort_students.h"

struct StudentName {
    std::string name, surname;
};

struct Date {
    int year, month, day;
};

std::map<std::string, std::vector<StudentName>> GetStudents(
        const std::vector<std::pair<std::string, int>>& university_info,
        const std::vector<std::tuple<StudentName, Date, int, std::vector<std::string>>>& students_info) {
    std::map<std::string, std::vector<StudentName>> answer;
    std::map<std::string, std::vector<Student>> list_of_admitted_students;
    std::map<std::string, int> universities_place_num;
    for (auto university : university_info) {
        auto name = get<0>(university);
        auto place_num = get<1>(university);
        list_of_admitted_students[name] = {};
        answer[name] = {};
        universities_place_num[name] = place_num;
    }
    std::vector<Student> students;
    for (auto s : students_info) {
        auto name = get<0>(s);
        auto date = get<1>(s);
        auto score = get<2>(s);
        auto universities = get<3>(s);
        Student student;
        student.surname = name.surname;
        student.name = name.name;
        student.year = date.year;
        student.month = date.month;
        student.day = date.day;
        student.score = score;
        student.universities = universities;
        students.push_back(student);
    }
    SortStudents(&students, SortType::kByScore);
    for (auto student : students) {
        bool university_chosen = false;
        int universities_viewed = 0;
        while (!university_chosen && universities_viewed < student.universities.size()) {
            auto university_name = student.universities[universities_viewed];
            if (universities_place_num[university_name]) {
                list_of_admitted_students[university_name].push_back(student);
                university_chosen = true;
                --universities_place_num[university_name];
            }
            ++universities_viewed;
        }
    }
    for (auto us : list_of_admitted_students) {
        auto university_name = us.first;
        auto student = us.second;
        SortStudents(&list_of_admitted_students[university_name], SortType::kByName);
        for (auto student : list_of_admitted_students[university_name]) {
            StudentName student_name;
            student_name.surname = student.surname;
            student_name.name = student.name;
            answer[university_name].push_back(student_name);
        }
    }

    return answer;
}
