#include <iostream>
#include "entrance.h"

int main() {
    Student a;
    a.score = 300;
    a.surname = "aa";
    a.name = "a";
    a.year = 1997;
    a.month = 11;
    a.day = 26;
    a.universities = {"MIPT", "HSE", "MSU"};

    Student b;
    b.score = 300;
    b.surname = "bb";
    b.name = "b";
    b.year = 1997;
    b.month = 12;
    b.day = 31;
    b.universities = {"MIPT", "HSE", "MSU"};

    Student c;
    c.score = 299;
    c.surname = "cc";
    c.name = "c";
    c.year = 1996;
    c.month = 10;
    c.day = 25;
    c.universities = {"MIPT", "MSU", "HSE"};

    Student d;
    d.score = 300;
    d.surname = "aa";
    d.name = "d";
    d.year = 1997;
    d.month = 11;
    d.day = 26;
    d.universities = {"ITMO", "SPBSU"};

    Student e;
    e.score = 290;
    e.surname = "aa";
    e.name = "a";
    e.year = 1997;
    e.month = 11;
    e.day = 25;
    e.universities = {"ITMO", "SPBSU", "MIPT", "HSE", "MSU"};

    Student f;
    f.score = 1;
    f.surname = "ff";
    f.name = "f";
    f.year = 1996;
    f.month = 10;
    f.day = 25;
    f.universities = {"MIPT", "HSE", "MSU"};

    Student g;
    g.score = 100;
    g.surname = "ff";
    g.name = "f";
    g.year = 1996;
    g.month = 10;
    g.day = 25;
    g.universities = {"MIPT"};

    Student h;
    h.score = 100;
    h.surname = "hh";
    h.name = "h";
    h.year = 1996;
    h.month = 10;
    h.day = 25;
    h.universities = {"MIPT", "ITMO", "HSE", "MSU", "SPBSU"};

    std::vector<Student> students = {a, b, c, d, e, f, g, h};

    std::vector<std::pair<std::string, int>> university_info;
    university_info.emplace_back("HSE", 2);
    university_info.emplace_back("MIPT", 2);
    university_info.emplace_back("MSU", 1);
    university_info.emplace_back("ITMO", 1);
    university_info.emplace_back("SPBSU", 2);
    university_info.emplace_back("NSU", 2);

    std::vector<std::tuple<StudentName, Date, int, std::vector<std::string>>> students_info;
    for (auto student : students) {
        StudentName student_name;
        Date date;
        int score;
        std::vector<std::string> universities_list;
        student_name.surname = student.surname;
        student_name.name = student.name;
        date.year = student.year;
        date.month = student.month;
        date.day = student.day;
        score = student.score;
        universities_list = student.universities;
        students_info.emplace_back(student_name, date, score, universities_list);
    }

    auto ans = GetStudents(university_info, students_info);
    for (auto us : ans) {
        auto university_name = us.first;
        auto students = us.second;
        std::cout << university_name << " :" << std::endl;
        for (auto student : students) {
            std::cout << student.surname << " " << student.name << std::endl;
        }
        std::cout << std::endl;
    }

    return 0;
}
