#pragma once

#include <stdexcept>
#include <string>
#include <vector>

struct Student {
    std::string name, surname;
    int year, month, day;
    int score;
    std::vector<std::string> universities;
};

enum class SortType { kByName, kByScore };

bool CmpByScore(Student a, Student b) {
    if (!(a.score == b.score)) {
        return a.score > b.score;
    } else if (!(a.year == b.year)) {
        return a.year < b.year;
    } else if (!(a.month == b.month)) {
        return a.month < b.month;
    } else if (!(a.day == b.day)) {
        return a.day < b.day;
    } else if (!(a.surname == b.surname)) {
        return a.surname < b.surname;
    } else {
        return a.name < b.name;
    }
}

bool CmpByName(Student a, Student b) {
    if (!(a.surname == b.surname)) {
        return a.surname < b.surname;
    } else if (!(a.name == b.name)) {
        return a.name < b.name;
    } else if (!(a.year == b.year)) {
        return a.year < b.year;
    } else if (!(a.month == b.month)) {
        return a.month < b.month;
    } else {
        return a.day < b.day;
    }
}

inline void SortStudents(std::vector<Student> *students, SortType sort_type) {
    if (sort_type == SortType::kByScore) {
        std::sort(students->begin(), students->end(), CmpByScore);
    } else if (sort_type == SortType::kByName) {
        std::sort(students->begin(), students->end(), CmpByName);
    }
}
