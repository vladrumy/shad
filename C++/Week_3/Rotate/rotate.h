#pragma once

#include <stdexcept>
#include <vector>

void Rotate(std::vector<int> *data, size_t shift) {
    auto n = (*data).size();
//    v[0] = 0;
    if (2 * shift < n) {
        for (int i = shift; i < n; ++i) {
            std::swap((*data)[i], (*data)[i - shift]);
        }
    } else {
        for (int i = n - 1; i >= n - shift; --i) {
            std::swap((*data)[i], (*data)[i + shift - n]);
        }
    }

}
