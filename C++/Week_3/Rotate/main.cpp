#include <iostream>
#include "rotate.h"

int main() {
    int n, shift;
    std::cin >> n;
    std::vector<int> v(n);
    for (auto i = 0; i < n; ++i) {
        std::cin >> v[i];
    }
    std::cin >> shift;
    Rotate(&v, shift);
    for (auto elem : v) {
        std::cout << elem << " ";
    }
    std::cout << std::endl;

    return 0;
}
