#pragma once

#include <stdexcept>

int64_t Multiply(int a, int b) {
//    throw std::runtime_error("Not implemented");
    auto c = static_cast<int64_t> (a);
    auto d = static_cast<int64_t> (b);

    return c * d;
}
