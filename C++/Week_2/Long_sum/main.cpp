#include <iostream>
#include "long_sum.h"

int main() {
    std::string a, b;
    std::cin >> a >> b;
    std::cout << LongSum(a, b) << std::endl;

    return 0;
}
