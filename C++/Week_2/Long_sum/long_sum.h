#pragma once

#include <iomanip>
#include <string>
#include <stdexcept>
#include <utility>

int stoint(char s) {
    return (int)s - (int)'0';
}


inline std::string LongSum(const std::string& a, const std::string& b) {
    int c = 0;
    std::string ans;
    int i = 0;
    while (a.length() > i && b.length() > i) {
        ++i;
        c += stoint(a[a.length() - i]) + stoint(b[b.length() - i]);
//        std::cout << a[a.length() - i] << " " << b[b.length() - i] << " - ";
//        std::cout << (int)a[a.length() - i] << " " << (int)b[b.length() - i] << " - ";
        ans = std::to_string(c % 10) + ans;
//        std::cout << c << std::endl;
        c /= 10;
    }
    while (a.length() > i) {
        ++i;
        c += stoint(a[a.length() - i]);
//        std::cout << a[a.length() - i] << " " << b[b.length() - i] << " - ";
//        std::cout << (int)a[a.length() - i] << " " << (int)b[b.length() - i] << " - ";
        ans = std::to_string(c % 10) + ans;
//        std::cout << c << std::endl;
        c /= 10;
    }
    while (b.length() > i) {
        ++i;
        c += stoint(b[b.length() - i]);
//        std::cout << a[a.length() - i] << " " << b[b.length() - i] << " - ";
//        std::cout << (int)a[a.length() - i] << " " << (int)b[b.length() - i] << " - ";
        ans = std::to_string(c % 10) + ans;
//        std::cout << c << std::endl;
        c /= 10;
    }

    if (c) {
        ans = std::to_string(c) + ans;
    }

    return ans;
}
