#include <iostream>
#include "reverse_map.h"

int main() {
    std::unordered_map<std::string, int> map = {};
    auto ans = ReverseMap(map);
    for (auto it : ans) {
        std::cout << it.first << " " << it.second << std::endl;
    }

    return 0;
}
