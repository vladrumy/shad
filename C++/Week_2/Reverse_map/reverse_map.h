#pragma once

#include <string>
#include <unordered_map>
#include <stdexcept>

std::unordered_map<int, std::string> ReverseMap(std::unordered_map<std::string, int> map) {
    std::unordered_map<int, std::string> ans;
    for (auto it : map) {
        ans[it.second] = it.first;
    }

    return ans;
}
