#pragma once

#include <string>
#include <stdexcept>
#include <unordered_set>

int DifferentWordsCount(const std::string& str) {
//    throw std::runtime_error("Not implemented");
    std::unordered_set<std::string> words;
    int i = 0;
    while (i < str.length()) {
        char cur_char = str[i];
        std::string cur_word = "";
        while (std::isalpha(cur_char)) {
            cur_word += std::tolower(cur_char);
            ++i;
            cur_char = str[i];
        }
        if (cur_word != "") {
            words.insert(cur_word);
        }
        ++i;
    }

    return words.size();
}

