#include <iostream>
#include <string>
#include <vector>
#include "word_count.h"

int main() {
    std::string str, del;
    std::getline(std::cin, str);
    auto ans = DifferentWordsCount(str);
    std::cout << ans << std::endl;

    return 0;
}
