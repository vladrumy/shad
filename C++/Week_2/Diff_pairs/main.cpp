#include <iostream>
#include "diff_pairs.h"

int main() {
    int x, n;
    std::cin >> x >> n;
    std::vector<int> vec(n);
    for (int i = 0; i < n; ++ i) {
        std::cin >> vec[i];
    }
    std::cout << CountPairs(vec, x);

    return 0;
}
