cmake_minimum_required(VERSION 3.21)
project(Count_pairs)

set(CMAKE_CXX_STANDARD 20)

add_executable(Count_pairs main.cpp Count_pairs.h)
