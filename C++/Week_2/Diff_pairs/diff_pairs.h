#pragma once

#include <stdexcept>
#include <vector>
#include <unordered_map>

inline int64_t CountPairs(const std::vector<int>& data, int x) {
    int ans = 0;
    std::unordered_map<int, int> map;
    for (auto elem : data) {
        ans += map[x - elem];
        ++map[elem];
    }

    return ans;
}
