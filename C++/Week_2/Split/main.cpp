#include <iostream>
#include <string>
#include <vector>
#include "split.h"

int main() {
    std::string str, del;
    std::getline(std::cin, str);
    std::getline(std::cin, del);
    auto ans = Split(str, del);
    std::cout << ans.size() << std::endl;
    for (auto elem : ans) {
        std::cout << elem << std::endl;
    }

    return 0;
}
