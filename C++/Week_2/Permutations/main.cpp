#include <iostream>
#include "permutations.h"


int main() {
    int n;
    std::cin >> n;
    auto permutations = GeneratePermutations(n);
    for (auto v : Permutations) {
        for (auto elem : v) {
            std::cout << elem << "";
        }
        std::cout << std::endl;
    }

    return 0;
}
