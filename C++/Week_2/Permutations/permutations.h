#pragma once

#include <numeric>
#include <set>
#include <vector>


std::vector<std::vector<int>> Permutations;

void permute(std::vector<int> prefix, std::set<int> rest) {
    if (rest.size() == 0) {
        Permutations.push_back(prefix);
        return;
    }
    for (auto elem : rest) {
        auto prefix_ = prefix;
        prefix_.push_back(elem);
        auto rest_= rest;
        rest_.erase(elem);
        permute(prefix_, rest_);
    }
}

std::vector<std::vector<int>> GeneratePermutations(size_t len) {
    std::vector<int> num(len);
    std::iota(num.begin(), num.end(), 0);
    std::set<int> s(num.begin(), num.end());
    permute({}, s);

    return {};
}
