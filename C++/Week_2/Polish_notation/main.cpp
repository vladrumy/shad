#include <iostream>
#include <string>
#include "polish_notation.h"

int main() {
    std::string expression;
    std::getline(std::cin, expression);

    std::cout << EvaluateExpression(expression) << std::endl;

    return 0;
}
