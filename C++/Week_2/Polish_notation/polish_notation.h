#pragma once

#include <string>
#include <stdexcept>
#include <stack>
#include <string>
#include <vector>
#include "split.h"

int EvaluateExpression(const std::string& expression) {
//    throw std::runtime_error("Not implemented");
    auto exp_split = Split(expression);
    std::stack<int> numbers;
    numbers.push(std::stoi(exp_split[0]));
    int a;
    for (auto i = 1; i < exp_split.size(); ++i) {
        auto cur = exp_split[i];
        if (cur == "+") {
            a = numbers.top();
            numbers.pop();
            a += numbers.top();
            numbers.pop();
            numbers.push(a);
        } else if (cur == "*") {
            a = numbers.top();
            numbers.pop();
            a *= numbers.top();
            numbers.pop();
            numbers.push(a);
        } else if (cur == "-") {
            a = numbers.top();
            numbers.pop();
            a = numbers.top() - a;
            numbers.pop();
            numbers.push(a);
        } else {
            numbers.push(stoi(cur));
        }
    }

    return numbers.top();
}
