#pragma once

#include <string>
#include <vector>

std::vector<std::string> Split(std::string str, std::string del = " ") {
    std::vector<std::string> ans;
    unsigned long l = 0;
    auto r = str.find(del);
    while (r != std::string::npos) {
        ans.push_back(str.substr(l, r - l));
        l = r + del.length();
        r = str.find(del, l);
    }
    ans.push_back(str.substr(l, r - l));

    return ans;
}
