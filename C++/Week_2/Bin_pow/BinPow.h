#pragma once

#include <stdexcept>

int BinPow(int a, int64_t b, int c) {
//    throw std::runtime_error("Not implemented");
    auto a_ = static_cast<int64_t> (a);
    auto c_ = static_cast<int64_t> (c);
    int64_t ans = 1;
    int i = 0;
    while (b > 0) {
        ++i;
        if (b % 2) {
//            std::cout << i << " " << a_ << " " << ans << std::endl;
            ans *= a_;
            ans %= c;
        }
//        std::cout << i << " " << a_ << " " << ans << std::endl;
        a_ = a_ * a_;
        a_ = a_ % c;
        b /= 2;
//        b >>= 1;
    }

    return ans;
}
