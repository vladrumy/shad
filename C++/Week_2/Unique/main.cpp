#include <iostream>
#include <vector>
#include "unique.h"


int main() {
    std::vector<int> vec = {0};
    auto ans = Unique(vec);
    for (auto i = 0; i < ans.size(); ++i) {
        std::cout << ans[i] << " ";
    }
    std::cout << std::endl;
}
