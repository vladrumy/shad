#pragma once

#include <vector>
#include <stdexcept>


std::vector<int> Unique(std::vector<int>& data) {
//    throw std::runtime_error("Not implemented");
    if (!data.size()) {
        return {};
    }
    std::vector<int> ans = {data[0]};
    for (auto cur : data) {
        if (ans[ans.size() - 1] != cur) {
            ans.push_back(cur);
        }
    }

    return ans;
}
