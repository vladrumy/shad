#include <iostream>
#include <vector>
#include "range.h"

int main() {
    int from, to, step;
    std::cin >> from >> to >> step;
    std::vector<int> ans = Range(from, to, step);
    for (int i = 0; i < ans.size(); ++i) {
        std::cout << ans[i] << " ";
    }

    return 0;
}
