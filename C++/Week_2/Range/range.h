#pragma once

#include <vector>
#include <stdexcept>

template <typename T> int Sign(T val) {
    return (T(0) < val) - (val < T(0));
}

std::vector<int> Range(int from, int to, int step = 1) {
    //    throw std::runtime_error("Not implemented");
    std::vector<int> ans;
    int sign = Sign(step);

    while (sign * from < sign * to) {
        ans.push_back(from);
        from += step;
    }

    return ans;
}
