#pragma once

#include <vector>
#include <string>
#include <utility>

class StaticMap {
public:
    explicit StaticMap(const std::vector<std::pair<std::string, std::string>>& items)
        : items_(items) {
        Sort();
    }

    struct compare {
        bool operator()(const std::pair<std::string, std::string>& value,
                        const std::string& key)
        {
            return (value.first < key);
        }
        bool operator()(const int& key,
                        const std::pair<int, int>& value)
        {
            return (key < value.first);
        }
    };

    bool Find(const std::string& key, std::string* value) const {
        auto value_ptr = std::lower_bound(items_.begin(), items_.end(), key, compare());
        if (value_ptr == items_.end() || key != value_ptr->first) {
            return false;
        }
        *value = value_ptr->second;
        return true;
    }

private:
    std::vector<std::pair<std::string, std::string>> items_;

    void Sort() {
        std::sort(items_.begin(), items_.end());
    }
};
