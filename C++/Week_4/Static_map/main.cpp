#include <algorithm>
#include <iostream>
#include <random>
#include "static_map.h"


int main() {
    StaticMap map({});
    std::string value;
    std::cout << (false == map.Find("key", &value)) << std::endl;
    std::cout << std::endl;

    StaticMap map_1({{"b", "1"}, {"a", "2"}});
    std::cout << (true == map_1.Find("b", &value)) << std::endl;
    std::cout << ("1" == value) << std::endl;
    std::cout << (true == map_1.Find("a", &value)) << std::endl;
    std::cout << ("2" == value) << std::endl;
    std::cout << (false == map_1.Find("c", &value)) << std::endl;
    std::cout << (false == map_1.Find("0", &value)) << std::endl;
    std::cout << std::endl;

    const int num_elements = 10;
    std::vector<std::pair<std::string, std::string>> items;
    for (int i = 0; i < num_elements; ++i) {
        items.emplace_back(std::to_string(i), std::to_string(i));
    }
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(items.begin(), items.end(), g);
    StaticMap map_2(items);
    for (int i = 0; i < num_elements; ++i) {
        std::string value;
        std::cout << (true == map_2.Find(std::to_string(i), &value)) << std::endl;
        std::cout << (std::to_string(i) == value) << std::endl;
    }


    return 0;
}