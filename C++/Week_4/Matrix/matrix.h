#pragma once
#include <iostream>
#include <string>
#include <vector>

class Matrix {
public:
    Matrix(int rows, int columns)
        : rows_(rows)
        , columns_(columns) {
        Set_size();
    };

    Matrix(int rows)
        : rows_(rows)
        , columns_(rows) {
        Set_size();
    };

    Matrix(std::vector<std::vector<double>> matrix)
        : rows_(matrix.size())
        , columns_(matrix.empty() ? 0 : matrix[0].size())
        , matrix_(matrix) {};

    int Rows() const {
        return rows_;
    }

    int Columns() const {
        return columns_;
    }

    void Cout() const {
        for (auto row : matrix_) {
            for (auto elem : row) {
                std::cout << elem << " ";
            }
            std::cout << std::endl;
        }
    }

    double operator()(int row, int column) const {
        return matrix_[row][column];
    }

    double& operator()(int row, int column) {
        return matrix_[row][column];
    }

    Matrix& operator+=(const Matrix& second) {
        for (auto i = 0; i < this->rows_; ++i) {
            for (auto j = 0; j < this->columns_; ++j) {
                this->matrix_[i][j] += second.matrix_[i][j];
            }
        }
        return *this;
    }

    friend Matrix operator+(const Matrix& first, const Matrix& second) {
        auto sum = first;
        sum += second;
        return sum;
    }

    Matrix& operator-=(const Matrix& second) {
        for (auto i = 0; i < this->rows_; ++i) {
            for (auto j = 0; j < this->columns_; ++j) {
                this->matrix_[i][j] -= second.matrix_[i][j];
            }
        }
        return *this;
    }

    friend Matrix operator-(const Matrix& first, const Matrix& second) {
        auto diff = first;
        diff -= second;
        return diff;
    }

    friend Matrix operator*(const Matrix& first, const Matrix& second) {
//        if (first.columns_ != first.rows_) {
//            std::cout << "ERROR : " << first.columns_ << " " << second.rows_ << std::endl;
//            return first;
//        }
        Matrix answer(first.rows_, second.columns_);
        answer.Set_size();
        for (auto i = 0; i < first.rows_; ++i) {
            for (auto j = 0; j < second.columns_; ++j) {
                for (auto k = 0; k < first.columns_; ++k) {
                    answer.matrix_[i][j] += first.matrix_[i][k] * second.matrix_[k][j];
//                    this->matrix_[i][j] += second.matrix_[i][j];
                }
            }
        }
        return answer;
    }

    Matrix& operator*=(Matrix& second) {
        return *this = *this * second;
    }

    friend Matrix Transpose(const Matrix& matrix);

    friend Matrix Identity(int n);

private:
    int rows_, columns_;
    std::vector<std::vector<double>> matrix_;

    void Set_size() {
        matrix_.assign(rows_, std::vector<double>(columns_));
    }
};

Matrix Transpose(const Matrix& matrix) {
    Matrix transposed(matrix.Columns(), matrix.Rows());
    for (int i = 0; i < matrix.Columns(); ++i) {
        for (int j = 0; j < matrix.Rows(); ++j) {
            transposed.matrix_[i][j] = matrix.matrix_[j][i];
        }
    }
    return transposed;
}

Matrix Identity(int n) {
    Matrix i(n, n);
    for (int j = 0; j < n; ++j) {
        i.matrix_[j][j] = 1.0;
    }
    return i;
}
