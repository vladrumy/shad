#include <iostream>
#include "matrix.h"


bool EqualMatrix(const Matrix& a, const Matrix& b) {
    if (a.Rows() != b.Rows()) {
        return false;
    }
    if (a.Columns() != b.Columns()) {
        return false;
    }
    for (size_t i = 0; i < a.Rows(); ++i) {
        for (size_t j = 0; j < a.Columns(); ++j) {
            if (a(i, j) != (b(i, j))) {
                return false;
            }
        }
    }
    return true;
}

int main() {
//    Constructors
    Matrix a(3);
    std::cout << (3u == a.Rows()) << std::endl;
    std::cout << (3u == a.Columns()) << std::endl;
    Matrix b(3, 5);
    std::cout << (3u == b.Rows()) << std::endl;
    std::cout << (5u == b.Columns()) << std::endl;
    Matrix c({{1, 2}, {3, 4}, {5, 6}});
    std::cout << (3u == c.Rows()) << std::endl;
    std::cout << (2u == c.Columns()) << std::endl;
    std::cout << (3 == c(1, 0)) << std::endl;
    c(1, 0) = 1;
    std::cout << (1 == c(1, 0)) << std::endl;
    std::cout << std::endl;

//    Constness
    Matrix d({{1, 2}, {3, 4}});
    const Matrix& e = d;
    std::cout << (2u == e.Rows()) << std::endl;
    std::cout << (2u == e.Columns()) << std::endl;
    std::cout << (2 == e(0, 1)) << std::endl;
    const Matrix first = Identity(3);
    const Matrix second(3);
    std::cout << (true == EqualMatrix(first, Transpose(first))) << std::endl;
    std::cout << (true == EqualMatrix(second, first - first)) << std::endl;
    std::cout << (true == EqualMatrix(first, first * first)) << std::endl;
    std::cout << std::endl;

//    Operations
    Matrix a_({{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}});
    Matrix b_({{0.0, 1.0, 0.0}, {1.0, 1.0, 2.0}});
    Matrix c_({{-1.0, -1.0}, {1.0, 1.0}, {1.0, -1.0}});
    std::cout << (false == EqualMatrix(a, Transpose(a_))) << std::endl;
    std::cout << (true == EqualMatrix(Transpose(a_), Matrix({{1.0, 4.0}, {2.0, 5.0}, {3.0, 6.0}}))) << std::endl;
    Matrix old_a = a_;
    std::cout << (true == EqualMatrix(a_ += b_, Matrix({{1.0, 3.0, 3.0}, {5.0, 6.0, 8.0}}))) << std::endl;
    std::cout << (true == EqualMatrix(a_ -= b_, old_a)) << std::endl;
    std::cout << (true == EqualMatrix(a_ -= a_, Matrix(2, 3))) << std::endl;
    std::cout << (true == EqualMatrix(b_ * c_, Matrix({{1.0, 1.0}, {2.0, -2.0}}))) << std::endl;
}
