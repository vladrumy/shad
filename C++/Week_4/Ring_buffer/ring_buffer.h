#pragma once
#include <stddef.h>
#include <vector>

class RingBuffer {
public:
    explicit RingBuffer(size_t capacity)
    : capacity_(capacity) {}

    size_t Size() const {
        return v_.size();
    }

    bool Empty() const {
        return v_.empty();
    }

    bool TryPush(int element) {
        if (this->Size() < capacity_) {
            v_.push_back(element);
            return true;
        }
        return false;
    }

    void Cout() {
        for (auto elem : v_) {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }

    bool TryPop(int *element) {
        if (this->Empty()) {
            return false;
        }
        *element = v_[0];
//        std::cout << "DEBUG : " << &v_[0] << " " << v_[0] << " " << *element << std::endl;
//        this->Cout();
        for (auto i = 1; i < this->Size(); ++i) {
            v_[i - 1] = v_[i];
        }
        v_.pop_back();
        return true;
    }

private:
    size_t capacity_;
    std::vector<int> v_;
};
