#include <iostream>
#include "ring_buffer.h"

int main() {
    RingBuffer buffer(1);
    std::cout << (0u == buffer.Size()) << std::endl;
    std::cout << (true == buffer.Empty()) << std::endl;
    std::cout << std::endl;

    RingBuffer buffer_1(2);
    int i;
    std::cout << (true == buffer_1.TryPush(0)) << std::endl;
    std::cout << (true == buffer_1.TryPush(1)) << std::endl;
    std::cout << (false == buffer_1.TryPush(2)) << std::endl;
    std::cout << (2u == buffer_1.Size()) << std::endl;
    std::cout << (false == buffer_1.Empty()) << std::endl;
    std::cout << (true == buffer_1.TryPop(&i)) << std::endl;
    std::cout << (0 == i) << std::endl;
    std::cout << (true == buffer_1.TryPop(&i)) << std::endl;
    std::cout << (1 == i) << std::endl;
    std::cout << (false == buffer_1.TryPop(&i)) << std::endl;
    std::cout << (0u == buffer_1.Size()) << std::endl;
    std::cout << (true == buffer_1.Empty()) << std::endl;
    std::cout << std::endl;

    return 0;
}
