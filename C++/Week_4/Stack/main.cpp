#include <iostream>
#include "stack.h"

int main() {
    Stack s;
    s.Push(0);
    s.Push(1);
    s.Push(2);
    std::cout << s.Top() << std::endl;
    s.Pop();
    s.Cout();
    std::cout << s.Top() << std::endl;
    s.Pop();
    std::cout << s.Top() << std::endl;
    s.Pop();
    s.Cout();
    s.Pop();
    s.Cout();
    std::cout << std::endl;

    Stack t;
    std::cout << t.Empty() << std::endl;
    std::cout << t.Size() << std::endl;
    t.Push(1);
    std::cout << t.Empty() << std::endl;
    std::cout << t.Size() << std::endl;
    t.Pop();
    std::cout << std::endl;

    Stack u;
    const int iterations = 12;
    for (int i = 0; i < iterations; ++i) {
        u.Push(i);
    }
    for (int i = 0; i < iterations; ++i) {
        std::cout << (iterations - i - 1 == u.Top()) << std::endl;
        u.Pop();
        if (i == 6 || i == 8) {
            u.Cout();
        }
    }
    std::cout << u.Empty() << std::endl;
    std::cout << u.Size() << std::endl;

    return 0;
}
