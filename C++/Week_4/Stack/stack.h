#pragma once

#include <cstdint>
#include <vector>

class Stack {
public:
    Stack() {}

    void Push(int x) {
        v.push_back(x);
    }

    bool Pop() {
        if (!v.empty()) {
            v.pop_back();
        }
    }

    int Top() const {
        return v[v.size() - 1];
    }

    bool Empty() const {
        return v.empty();
    }

    size_t Size() const {
        return v.size();
    }

    void Cout() const{
        for (auto elem : v) {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
private:
    std::vector<int> v;
};
